package MyProject.servlets;

import MyProject.entity.Like;
import MyProject.entity.User;
import MyProject.service.LikeService;
import MyProject.template.TemplateEngine;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;

public class LikeServlet extends HttpServlet {
    private final TemplateEngine engine;
    private LikeService likeService;
    private User user;

    public LikeServlet(TemplateEngine engine) {
        this.engine=engine;
        likeService=new LikeService();
        user = likeService.firstPost();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        Cookie[] cookies = req.getCookies();
        for (Cookie oneCookie : cookies)
            if (oneCookie.getName().equals("%ID%"))
                likeService.setLocalId(Integer.parseInt(oneCookie.getValue()));
        if (user.getId() == likeService.getId()) {
            try {
                user = likeService.nextPost(user.getId());
                HashMap<String, Object> data = new HashMap<>();
                data.put("id", user.getId());
                data.put("username", user.getFullName());
                data.put("urlPhoto", user.getUrlPhoto());
                engine.render("like-page.ftl", data, resp);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String like = req.getParameter("like");
        if (like != null) {
            try {
                likeService.like(Integer.parseInt(like));
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        try {
            user = likeService.nextPost(user.getId());
            resp.sendRedirect("/like/*");
        } catch (SQLException ex) {
            resp.sendRedirect("/MainPage/*");
        }
    }
}
