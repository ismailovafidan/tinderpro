package MyProject.servlets;

import MyProject.entity.User;
import MyProject.service.RegisterService;
import MyProject.template.TemplateEngine;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

public class RegisterServlet extends HttpServlet {

    private final TemplateEngine engine;
    private RegisterService registerService;

    public RegisterServlet(TemplateEngine engine) {
        this.engine = engine;
        registerService = new RegisterService();
    }
    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {

        engine.render("register.ftl", resp);
    }
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {

        try { User user =new User(
                req.getParameter("username"),
                req.getParameter("gender"),
                req.getParameter("password"),
                req.getParameter("email"),
                req.getParameter("photoUrl"),
                req.getParameter("position")

        );

            registerService.register(user);
            System.out.println(user.toString() + "in register service");

            resp.sendRedirect("/login/");
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
}
