package MyProject.servlets;
import MyProject.entity.User;
import MyProject.service.MainPageService;
import MyProject.template.TemplateEngine;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

public class MainPageServlet extends HttpServlet {

    private final TemplateEngine engine;
    private final MainPageService mainPageService;
    protected int id;

    public MainPageServlet(TemplateEngine engine) {
        this.engine = engine;
        mainPageService =new MainPageService();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Cookie[] cookies = req.getCookies();
        for (Cookie cookie : cookies) {
            if (cookie.getName().equals("%ID%"))
                id = Integer.parseInt(cookie.getValue());
        }
        List<User> likedUsers = mainPageService.showUsers(id);
        likedUsers.forEach(System.out::println);
        HashMap<String, Object> data = new HashMap<>();
        data.put("likedUsers", likedUsers);
        engine.render("people-list.ftl", data, resp);

    }

}
