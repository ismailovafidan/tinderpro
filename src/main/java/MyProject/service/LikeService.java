package MyProject.service;


import MyProject.db.LikesDb;
import MyProject.db.UserDb;
import MyProject.entity.Like;
import MyProject.entity.User;

import java.sql.SQLException;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class LikeService {
    private final UserDb userDb;
    private final LikesDb likesDb;
    private int id;
    private boolean liked;
    private List<Integer> users;

    public int getId(){
        return id;
    }

    public LikeService() {
        userDb = new UserDb();
        likesDb = new LikesDb();
        liked = false;
        users = userDb.getAllId();
    }

    public void like(int userto) throws SQLException {
        liked = true;
        Like like = new Like(id, userto);
        if (!likesDb.getDb().contains(like)) {
            likesDb.save(like);
        }
    }

    public Stream<User> userHimself() {
        return userDb.stream().filter(user -> user.getId() != id);
    }
    public User firstPost() {
        return userHimself()
                .collect(Collectors.toList())
                .get(0);
    }

    public boolean lastPost() {
        return users.isEmpty();
    }

    public boolean isLiked() {
        return liked;
    }

    public User nextPost(int user_to) throws IndexOutOfBoundsException, SQLException {
        users.remove(Integer.valueOf(user_to));

        if (!lastPost()) return userDb.get(users.get(0));
        else if (isLiked()) throw new IndexOutOfBoundsException();
        else users = userDb.getAllId();
        return null;
    }

    public void setLocalId(int id) {
        this.id = id;
    }
}
