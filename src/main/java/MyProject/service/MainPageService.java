package MyProject.service;

import MyProject.db.LikesDb;
import MyProject.db.UserDb;
import MyProject.entity.Like;
import MyProject.entity.User;
import java.sql.SQLException;
import java.util.List;
import java.util.stream.Collectors;

public class MainPageService {
    private final UserDb userDb;
    private final LikesDb likesDb;


    public MainPageService() {
        userDb = new UserDb();
        likesDb = new LikesDb();

    }

    public List<User> showUsers(int id) {
        List<Like> likedUsers = displayLikedUsers(id);
        return userDb.stream()
                .filter(user -> likedUsers.contains(new Like(id, user.getId())))
                .collect(Collectors.toList());
    }


    public List<Like> displayLikedUsers(int id) {
        likesDb.read();
        return likesDb.stream()
                .filter(oneLike -> oneLike.getUserfrom() == id)
                .collect(Collectors.toList());


    }

}
