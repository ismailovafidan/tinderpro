package MyProject.service;

import MyProject.db.SqlConnection;
import MyProject.db.UserDb;
import MyProject.entity.User;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class RegisterService {

    public void register(User user) throws SQLException {
        UserDb userDb = new UserDb();
        userDb.save(user);
    }

    }
