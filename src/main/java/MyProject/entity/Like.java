package MyProject.entity;

public class Like {
    private int id;
    private int userfrom;
    private int userto;
    private boolean liked;

    public Like(int id, int userfrom, int userto, boolean liked) {
        this.id = id;
        this.userfrom = userfrom;
        this.userto = userto;
        this.liked=liked;
    }



    public Like(int userfrom, int userto, boolean liked) {
        this.userfrom = userfrom;
        this.userto = userto;
        this.liked=liked;
    }

    public Like(int userfrom, int userto) {
        this.userfrom = userfrom;
        this.userto = userto;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUserfrom() {
        return userfrom;
    }

    public void setUserfrom(int userfrom) {
        this.userfrom = userfrom;
    }

    public int getUserto() {
        return userto;
    }

    public void setUserto(int userto) {
        this.userto = userto;
    }

    public boolean isLiked() {
        return liked;
    }

    public void setLiked(boolean liked) {
        this.liked = liked;
    }

    @Override
    public String toString() {
        return "Like{" +
                "id=" + id +
                ", userfrom=" + userfrom +
                ", userto=" + userto +
                ", liked=" + liked +
                '}';
    }
}
