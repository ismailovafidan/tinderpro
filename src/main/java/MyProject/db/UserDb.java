package MyProject.db;

import MyProject.entity.Like;
import MyProject.entity.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

public class UserDb implements db<User> {
    SqlConnection sqlConnection;
    private List<User> users;

    public UserDb() {
        super();
        sqlConnection = new SqlConnection();
    }

    @Override
    public User get(int id) throws SQLException {
        PreparedStatement preparedStatement = sqlConnection.preparedStatement("SELECT * FROM users WHERE id =" + id);
        ResultSet resultSet = preparedStatement.executeQuery();
        String username = "";
        String gender = "";
        String password = "";
        String email = "";
        String photo = "";
        String position = "";


        while (resultSet.next()) {
            username = resultSet.getString("username");
            gender = resultSet.getString("gender");
            password = resultSet.getString("password");
            email = resultSet.getString("email");
            photo = resultSet.getString("photo");
            position = resultSet.getString("position");


        }
        return new User(username, gender, password, email, photo, position);
    }


    @Override
    public List<User> getAll() throws SQLException {
        PreparedStatement preparedStatement = sqlConnection.preparedStatement("SELECT * FROM users");
        ResultSet resultSet = preparedStatement.executeQuery();
        users = new ArrayList<>();

        while (resultSet.next()) {

            users.add(new User(
                    resultSet.getInt("id"),
                    resultSet.getString("username"),
                    resultSet.getString("gender"),
                    resultSet.getString("password"),
                    resultSet.getString("email"),
                    resultSet.getString("photo"),
                    resultSet.getString("position")
            ));
        }
        return users;
    }

    @Override
    public void save(User user) throws SQLException {
        PreparedStatement preparedStatement = sqlConnection
                .preparedStatement("INSERT INTO users (username,gender,password,email,photo, position) VALUES (?,?,?,?,?,?)");

        try {

            preparedStatement.setString(1, user.getFullName());
            preparedStatement.setString(2, user.getGender());
            preparedStatement.setString(3, user.getPassword());
            preparedStatement.setString(4, user.getEmail());
            preparedStatement.setString(5, user.getUrlPhoto());
            preparedStatement.setString(6, user.getPosition());

            preparedStatement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }


    public void read() {
        users = new LinkedList<>();
        try {

            PreparedStatement preparedStatement = sqlConnection.preparedStatement("SELECT * FROM users");
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                users.add(new User(
                        resultSet.getInt("id"),
                        resultSet.getString("username"),
                        resultSet.getString("gender"),
                        resultSet.getString("password"),
                        resultSet.getString("email"),
                        resultSet.getString("photo"),
                        resultSet.getString("position")
                ));}
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public List<Integer> getAllId(){
        read();
        List<Integer> allId=new LinkedList<>();
        users.forEach(user -> allId.add(user.getId()));
        return allId;
    }

    @Override
    public void update(User user) {

    }

    @Override
    public void delete(User user) {

    }

    public List<User> getDb(){
        return users;
    }

    @Override
    public int getId(String user) throws SQLException {
        PreparedStatement preparedStatement = sqlConnection.preparedStatement("SELECT * FROM users");
        try {
            ResultSet resultSet = preparedStatement.executeQuery();
            String email;
            int id;
            while (resultSet.next()) {
                id = resultSet.getInt("id");
                email = resultSet.getString("email");
                if (user.equalsIgnoreCase(email)) {
                    return id;
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return -1;
    }


    @Override
    public Optional<User> getOneByEmail(String email, String password) {
        try {
            PreparedStatement preparedStatement = sqlConnection.preparedStatement("Select * from users where email =?");
            preparedStatement.setString(1, email);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                if (resultSet.getString("password").equals(password)) {
                    User user = new User(resultSet.getInt("id"),
                            resultSet.getString("username"),
                            resultSet.getString("gender"),
                            resultSet.getString("password"),
                            resultSet.getString("email"),
                            resultSet.getString("photo"),
                            resultSet.getString("position"));
                    return Optional.of(user);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return Optional.empty();
    }


    public Stream<User> stream() {
        return users.stream();
    }


}

