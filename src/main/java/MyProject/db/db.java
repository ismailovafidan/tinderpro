package MyProject.db;

import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

public interface db <T> {
    T get(int id) throws SQLException;

    List<T> getAll() throws SQLException;

    void save(T t) throws SQLException;

    void update(T t);

    void delete(T t);

    int getId(String s) throws SQLException;
//    boolean add(A a, B b);
    Optional<T> getOneByEmail(String username, String password);

}
