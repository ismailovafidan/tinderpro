package MyProject.db;

import MyProject.entity.Like;
import MyProject.entity.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

public class LikesDb implements db<Like> {
    SqlConnection sqlConnection;
    private List<Like> allLikes;


    @Override
    public Like get(int id) throws SQLException {
        PreparedStatement preparedStatement = sqlConnection.preparedStatement("SELECT * FROM alllikes WHERE id = " + id);
        ResultSet resultSet = preparedStatement.executeQuery();

        int user_from = 0;
        int user_to = 0;
        boolean liked = false;

        while (resultSet.next()) {
            user_from = resultSet.getInt("userfrom");
            user_to = resultSet.getInt("userto");
            liked = resultSet.getBoolean("liked");
        }

        return new Like(user_from, user_to, liked);
    }

    @Override
    public List<Like> getAll() throws SQLException {
        PreparedStatement preparedStatement = sqlConnection.preparedStatement("SELECT * FROM alllikes");
        ResultSet resultSet = preparedStatement.executeQuery();
        allLikes = new ArrayList<>();

        while (resultSet.next()) {

            allLikes.add(new Like(
                    resultSet.getInt("userfrom"),
                    resultSet.getInt("userto"),
                    resultSet.getBoolean("liked")
            ));
        }

        return allLikes;
    }

    @Override
    public void save(Like like) throws SQLException {
        PreparedStatement preparedStatement = sqlConnection.preparedStatement("Insert into  alllikes (userfrom, userto, liked) values (?,?,?)");
        try {
            preparedStatement.setInt(1, like.getUserfrom());
            preparedStatement.setInt(2, like.getUserto());
            preparedStatement.setBoolean(3, like.isLiked());
            preparedStatement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    public void read() {
        allLikes = new LinkedList<>();
        try {

            PreparedStatement preparedStatement = sqlConnection.preparedStatement("SELECT * FROM alllikes");
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                allLikes.add(new Like(
                        resultSet.getInt("id"),
                        resultSet.getInt("userfrom"),
                        resultSet.getInt("userto"),
                        resultSet.getBoolean("liked")));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void update(Like like) {

    }

    @Override
    public void delete(Like like) {

    }

    public List<Like> getDb(){
        return allLikes;
    }

    @Override
    public int getId(String s) {
        return 0;
    }
    public Stream<Like> stream()
    {
        allLikes = new LinkedList<>();
        return allLikes.stream();
    }

    @Override
    public Optional<Like> getOneByEmail(String username, String password) {
        return Optional.empty();
    }
}
