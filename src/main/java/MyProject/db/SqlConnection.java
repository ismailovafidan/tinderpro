package MyProject.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class SqlConnection {
private final static String URL="jdbc:postgresql://localhost:5432/postgres";
private final static String Name="postgres";
private final static String Password="postgres";

    private  Connection connection;

    SqlConnection() {
        try {
            connection = DriverManager.getConnection(URL, Name, Password);
        } catch (SQLException ex) {
            throw new RuntimeException("Something went wrong", ex);
        }
    }


    public PreparedStatement preparedStatement (String sql) throws SQLException {
        return connection.prepareStatement(sql);
    }

}
