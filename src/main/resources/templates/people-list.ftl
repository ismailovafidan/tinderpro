<#include "css/style.css">
<#include "css/bootstrap.min.css">
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="img/favicon.ico">

    <title>People list</title>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css"
          integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link rel="stylesheet" href="css/style.css">
</head>
<body>

<div class="container">
    <div class="row">
        <div class="col-8 offset-2">
            <div class="panel panel-default user_panel">
                <div class="panel-heading">
                    <h3 class="panel-title">User List</h3>
                </div>
                <div class="panel-body">
                    <div class="table-container">
                        <table class="table-users table" border="0">
                            <tbody>
                            <#list alluser as user>
                                <tr>
                                    <td width="10">
                                        <div class="avatar-img">
                                            <img class="img-circle"
                                                 src=${user.getUrlPhoto()}/>  
                                        </div>

                                    </td>
                                    <td class="align-middle">
                                        ${user.getFullName()}
                                    </td>
                                    <td class="align-middle">
                                        ${user.getPosition()}
                                    </td>
                                    <td class="align-middle">
                                        <form  method="post" action="/like">
                                            <input type="hidden"  name="id" value="${user.getId()}" >
                                        <input type="submit" value="LIKE">
                                        </form>
                                    </td>
                                    <td class="align-middle">
                                        <form method="post" action="/dislike">
                                            <input type="hidden" name="id" value="${user.getId()}" >
                                        <input type="submit" value="DISLIKE">
                                        </form>
                                    </td>
                                </tr>
                            </#list>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div>
    <form action="LikeServlet" method="post">
        <input type="hidden"  name="id" value="1" >
        <input type="submit" value="MESSAGES">
    </form>
</div>
<div >
    <form action="UnlikeServlet"method="post">
        <input type="hidden" name="id" value="2" >
        <input type="submit" value="LOGOUT">
    </form>
</div>

</body>
</html>
